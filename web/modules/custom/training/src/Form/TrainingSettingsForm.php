<?php

namespace Drupal\training\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class Name.
 */
class TrainingSettingsForm extends ConfigFormBase {

  /**
   * Set the Form ID.
   */
  public function getFormId() {
    return 'training_admin_settings';
  }

  /**
   * Get Editable Config Names.
   */
  protected function getEditableConfigNames() {
    return [
      'training.settings',
    ];
  }

  /**
   * Default Method to Construct Form.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Get config object.
    $config = \Drupal::config('training.settings');

    // Get user object.
    $user = \Drupal::currentUser();

    // Get User name.
    $userName = $user->getUsername();

    // Create field: "Developer Name".
    $form['developer_name'] = [
      '#title' => $this->t("Developer Name"),
      '#type' => 'textfield',
      '#description' => $this->t('Insert the developer name.'),
      '#default_value' => $userName,
      '#disabled' => TRUE,
    ];

    // Create field "Full name".
    $form['full_name'] = [
      '#title' => $this->t("Full Name"),
      '#type' => 'textfield',
      '#default_value' => $config->get('full_name'),
    ];

    // Create field "Website".
    $form['website'] = [
      '#title' => $this->t("Website"),
      '#type' => 'url',
      '#default_value' => $config->get('website'),
    ];

    $form['city'] = [
      '#title' => $this->t("City"),
      '#type' => 'textfield',
      '#default_value' => $config->get('city'),
    ];

    $form['email'] = [
      '#title' => $this->t("Email"),
      '#type' => 'email',
      '#default_value' => $config->get('email'),
    ];

    $form['favorite_color'] = [
      '#title' => $this->t("Favorite Color"),
      '#type' => 'radios',
      '#default_value' => $config->get('favorite_color'),
      '#options' => [
        'blue' => $this->t('Blue'),
        'green' => $this->t('Green'),
        'red' => $this->t('Red'),
        'other' => $this->t('Other'),
      ],
    ];

    $form['another_color'] = [
      '#type' => 'textfield',
      '#default_value' => $config->get('another_color'),
      '#states' => [
        'visible' => [
          ':input[name="favorite_color"]' => ['value' => 'other'],
        ],
      ],
    ];

    // Create submit.
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * Validate Form.
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    $fullName = $form_state->getValue('full_name');

    // Remove blank spaces.
    $fullName = trim($fullName);

    $form_state->setValue('full_name', $fullName);

    if (strlen($fullName) < 3) {
      $form_state->setErrorByName('full_name', $this->t('This full-name is so short. Please enter a valid name.'));
    }

    // Accept only letters on full-name.
    $validFullName = preg_match("/^[a-záàâãéèêíïóôõöúçñ ]+$/i", $fullName);

    // Valid if there is a space on full name.
    if (strpos($fullName, ' ') === FALSE) {
      $form_state->setErrorByName('full_name', $this->t('Plese insert your full name (First Name and Family Name)'));
    }

    if (!$validFullName) {
      $form_state->setErrorByName('full_name', $this->t('Use only letters on full name.'));
    }

    $website = $form_state->getValue('website');

    // Validate website.
    $validWebsite = preg_match('#((https?|ftp)://(\S*?\.\S*?))([\s)\[\]{},;"\':<]|\.\s|$)#i', $website);

    if ($validWebsite == FALSE) {
      $form_state->setErrorByName('website', $this->t('Please enter a valid website'));
    }

  }

  /**
   * Default Method to Submit Form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    // Get Config Object.
    $config = $this->config('training.settings');

    // Get the developer name on form.
    $developerName = $form_state->getValue('developer_name');

    // Get Full Name.
    $fullName = $form_state->getValue('full_name');

    // Get Website.
    $website = $form_state->getValue('website');

    // Get City.
    $city = $form_state->getValue('city');

    // Get Email.
    $email = $form_state->getValue('email');

    // Get Favorite Color.
    $favoriteColor = $form_state->getValue('favorite_color');

    // Get another Color (if exists).
    if (!empty($form_state->getValue('another_color'))) {

      $anotherColor = $form_state->getValue('another_color');

      $config->set('another_color', $anotherColor);
    }

    // If favorite color is different than "other" remove this value on
    // database.
    if ($favoriteColor != 'other') {
      $config->delete('another_color');
    }

    // Set developer Name on config Object.
    $config->set('developer_name', $developerName);

    // Set Full name on config object.
    $config->set('full_name', $fullName);

    // Set website on config object.
    $config->set('website', $website);

    // Set city on config object.
    $config->set('city', $city);

    // Set email on config object.
    $config->set('email', $email);

    // Set favorite color on config object.
    $config->set('favorite_color', $favoriteColor);

    // Save object config.
    $config->save();

    // Set a message with success. There is a token called @developer_name that
    // replace this value with developer name.
    $this->messenger()->addMessage($this->t('Thank you, your developer @developer_name has been saved', ['@developer_name' => $config->get('developer_name')]), 'status');

    // Submit Form.
    parent::submitForm($form, $form_state);
  }

}
