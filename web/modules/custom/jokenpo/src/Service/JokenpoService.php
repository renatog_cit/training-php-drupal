<?php

namespace Drupal\jokenpo\Service;

/**
 * Jokenpo Service Class.
 */
class JokenpoService {

  /**
   * Method to get Ranking.
   */
  public function getRanking() {

    $ranking = [];
    $playerScore = $computerScore = $tieScore = 0;

    $config = \Drupal::config('jokenpo.settings');

    $playerScore = $config->get('player_score');
    $computerScore = $config->get('computer_score');
    $tieScore = $config->get('tie_score');

    $ranking['player_score'] = $playerScore;
    $ranking['computer_score'] = $computerScore;
    $ranking['tie_score'] = $tieScore;

    return $ranking;
  }

}
