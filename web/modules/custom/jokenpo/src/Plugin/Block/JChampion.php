<?php

namespace Drupal\jokenpo\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a Jokenpo winner block.
 *
 * @Block(
 *    id="jchampion_block",
 *    admin_label=@Translation("Jokenpo Champion"),
 *    category=@Translation("Ranking")
 * )
 */
class JChampion extends BlockBase {

  /**
   * Function to build a block.
   */
  public function build() {

    // We have to check how to Dep Inj with BlockBase @codingStandardsIgnoreLine
    $champion = \Drupal::service('jokenpo.champion');
    $champName = self::getPlayerName($champion->getChampion());
    if ($champName == 'Nobody') {
      return [];
    }

    return [
      '#type' => 'markup',
      '#markup' => '<div class="js-jokenpo-champion">' . $this->t('Jokenpo champion: @champion_name', ['@champion_name' => $champName]) . '</div>',
    ];
  }

  /**
   * This function disables the block from being cached.
   */
  public function getCacheMaxAge() {
    return 0;
  }

  /**
   * A function that will get user's full name if it's the winner.
   */
  public function getPlayerName($champName) {

    // We have to check how to Dep Inj with BlockBase @codingStandardsIgnoreLine
    if (\Drupal::moduleHandler()->moduleExists('training') && empty(\Drupal::config('training.settings')->get('full_name')) != TRUE && $champName == "Player") {

      // We have to check how to Dep Inj with BlockBase @codingStandardsIgnoreLine
      $configTraining = \Drupal::config('training.settings');

      $champName = $configTraining->get('full_name');
    }
    return $champName;

  }

}
